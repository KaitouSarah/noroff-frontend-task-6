# Noroff Frontend Task 6

Follow the slides and create a website where the user may dynamically search for images from pexels.com.

* The website should serve the images dynamically from the Pexels API using jQuery.

* The website should look professional and make use of Twitter Bootstrap 4.

* The website should be deployed using Heroku or a similar hosting solution where it is publicly addressable online.

Candidates should submit a link to their git repository and the link to their deployed website.