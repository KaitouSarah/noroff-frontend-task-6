import '../styles/index.scss';
import * as moment from 'moment';

console.log('webpack starterkit');
console.log(moment().format('LLL'));

const apiKey = "563492ad6f917000010000011a2eb8127375433ea9de5eff880897f9";
var searchWord = "nature";
var card;

//
$(document).ready(function() {
    $.ajax({
        headers: {Authorization: `Bearer ${apiKey}`}, 
        url: 'https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query='+searchWord+'&per_page=15&page=1',
        type: 'GET'
    }).done(data => {
        renderImages(data.photos);
    });
});

//Renders all images into cards
function renderImages(photos) {
    $.each(photos, (i, photo)=>{
        card = createCard(photo);
        $(".card-group .row").append(card);
    });
} 

//Makes a html card with photographer name
function createCard(photo) {
    return `
    <div class="col-4">
    <div class="card h-100" style="18rem;">
    <div class="card-img">
    <img class="card-img-top" src="${photo.src.medium}"/>
    </div>
   
    <div class="card-body">
    <h5>Photo by: <i>${photo.photographer}</i></h5>
    </div>
    </div>
    </div>
    `;
}

//Removes cards from page
function removeCards() {
    document.getElementById("row").innerHTML = "";
}

//Scroll to the buttom when button is clicked
$(".fixed-scroll-button").click(function() {
    $('html,body').animate({scrollTop: document.body.scrollHeight},"slow");
});

//Scroll to the buttom when button is clicked
$(".button-search").click(function() {
    
    searchWord = document.getElementById("search-input").value;

    //Remove any alert message from before
    document.getElementById("alert-message").innerHTML = "";

    //Remove any previously entered search words
    document.getElementById("search-input").value = "";

    if (searchWord === "") {
        $("header .alert-message").append(`
            <div class="alert alert-danger">
                <strong>Error!</strong> You need to enter a search word.
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        `);
    } else {
        $.ajax({
            headers: {Authorization: `Bearer ${apiKey}`}, 
            url: 'https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query='+searchWord+'&per_page=15&page=1',
            type: 'GET'
        }).done(data => {
            if (data.photos.length === 0) {
                $("header .alert-message").append(`
                <div class="alert alert-warning">
                    <strong>Error!</strong> The search for ${searchWord} gave no results.
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            `);
            } else {
                removeCards();
                renderImages(data.photos);
            }
        });
    }
});